using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BetManager : MonoBehaviour
{
    public static BetManager betSystem;
    public int totalBetAmount;
    public Text TableBetAmount;
    public Text playerStartAmount;
    public int totalplayerStartAmount;

    private void Start() 
    {

        playerStartAmount.text = totalplayerStartAmount.ToString();
        
    }
    public void onBet(int betAmount)
    {

        totalBetAmount = betAmount + totalBetAmount;
        totalplayerStartAmount -= betAmount;
        OnBetUpdate();

    }

    public void OnBetUpdate()
    {

        TableBetAmount.text = totalBetAmount.ToString();
        playerStartAmount.text = totalplayerStartAmount.ToString();

    }

    public void BlackJackWin()
    {

        float Winnings = totalBetAmount * 1.5f;
        totalBetAmount += (int)Winnings;
        TableBetAmount.text = totalBetAmount.ToString();
        playerStartAmount.text = totalplayerStartAmount.ToString();
        Debug.Log("BlackJackWinPlayer");

    } 

    public void StandardWin()
    {

        float Winnings = totalBetAmount * 1.0f;
        totalBetAmount += (int)Winnings;
        TableBetAmount.text = totalBetAmount.ToString();
        playerStartAmount.text = totalplayerStartAmount.ToString();
        Debug.Log("StandardWinPlayer");

    }

    public void Draw()
    {
        Debug.Log("Draw");
    }

    public void DealerWin()
    {
        Debug.Log("DealerWin");
    }

    public void DealerBlackJack()
    {
        Debug.Log("DealerBlackJack");
    }

    private void Awake() 
    {
        betSystem = this;    
    }
}
